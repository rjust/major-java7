package major.ant;

/**
 * Wrapper class that indicates the kill reason for a given mutant ID.
 */
public class MutantKillResult {

    public enum KillReason {FAIL, EXC, TIME};

    private int mutantNo;
    private String testName;
    private KillReason reason;

    public MutantKillResult(int mutantNo, String testName, KillReason reason) {
        this.mutantNo = mutantNo;
        this.testName = testName;
        this.reason = reason;
    }

    public int getMutantNo() {
        return mutantNo;
    }

    public KillReason getKillReason() {
        return reason;
    }

    @Override
    public int hashCode() {
        return (testName + "--" + mutantNo).hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (! (obj instanceof MutantKillResult)) {
            return false;
        }
        MutantKillResult other = (MutantKillResult) obj;
        return other.testName.equals(this.testName) && other.mutantNo==this.mutantNo;
    }
}
