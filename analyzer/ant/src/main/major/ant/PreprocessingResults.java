package major.ant;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.tools.ant.taskdefs.optional.junit.JUnitTask.TestOrder;
import org.apache.tools.ant.taskdefs.optional.junit.JUnitTest;

/**
 * A wrapper class for serializing preprocessing results.
 * In addition to the coverage and test maps, this wrapper stores the test order,
 * which was used to generate the test and coverage map.
 *
 * After deserialization, the client code should verify that the number of
 * generated mutants and the test order configured in the analyzer match the
 * ones in the deserialized object. 
 */
public class PreprocessingResults implements Serializable {

  private static final long serialVersionUID = 1L;

  private int numGeneratedMutants;
  private Collection<JUnitTest> tests;
  private Map<String, List<Integer>> coverageMap;
  private TestOrder order;

  public PreprocessingResults(int numGeneratedMutants, Collection<JUnitTest> tests, Map<String, List<Integer>> coverageMap, TestOrder testOrder) {
    this.numGeneratedMutants = numGeneratedMutants;
    this.tests = tests;
    this.coverageMap = coverageMap;
    this.order = testOrder;
  }

  public Collection<JUnitTest> getTests() {
    return tests;
  }

  public Map<String, List<Integer>> getCoverageMap() {
    return coverageMap;
  }

  public TestOrder getTestOrder() {
    return order;
  }

  public int getNumGeneratedMutants() {
    return numGeneratedMutants;
  }
}
