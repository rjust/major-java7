package analysis;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.Result;
import prepass.TestMethod;
import utils.Utils;

public class TestRunnerTest {

    private static TestMethod testMethod;

    @BeforeClass
    public static void setup() {
        testMethod = Utils.getTestMethod("test1");
    }

    @Test
    public void runTestTest1() {
        Result result = TestRunner.runTest(testMethod, 1000, 1, true);
        Assert.assertTrue(result != null);
        Assert.assertTrue(result.wasSuccessful());
    }

    @Test
    public void runTestTest2() {
        Result result = TestRunner.runTest(testMethod, 0, 1, true);
        Assert.assertTrue(result == null);
    }

    @Test
    public void runTestTest3() {
        Result result = TestRunner.runTest(testMethod, 1000, 1, false);
        Assert.assertTrue(result != null);
        Assert.assertTrue(result.wasSuccessful());
    }

    @Test
    public void runTestTest4() {
        Result result = TestRunner.runTest(testMethod, 0, 1, false);
        Assert.assertTrue(result == null);
    }
}
