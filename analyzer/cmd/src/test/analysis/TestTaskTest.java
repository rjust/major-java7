package analysis;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import prepass.TestMethod;
import utils.Utils;

public class TestTaskTest {

    private static TestMethod testMethod;

    @BeforeClass
    public static void setup() {
        testMethod = Utils.getTestMethod("test1");
    }

    @Test
    public void callTest1() {
        try {
            Assert.assertTrue(new TestTask(testMethod, true).call().wasSuccessful());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void callTest2() {
        try {
            Assert.assertTrue(new TestTask(testMethod, false).call().wasSuccessful());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
