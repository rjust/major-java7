package prepass;

import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.net.URL;
import java.net.URLClassLoader;

public class TestFinderTest {

    @Test
    public void loadClassesTest() {
        try {
            String testDirectory = "triangle/build/test/triangle/test/";
            Assert.assertEquals("triangle.test.TriangleTest",
                    TestFinder.loadClasses(testDirectory).get(0).getName());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getTestMethodsTest() {
        try {
            Class<?> c = new URLClassLoader(new URL[] {
                    new File("triangle/build/main/").toURI().toURL(),
                    new File("triangle/build/test/").toURI().toURL()
            }).loadClass("triangle.test.TriangleTest");
            TestMethod testMethod = (TestMethod) TestFinder.getTestMethods(c, "triangle/build/main").toArray()[0];
            Assert.assertEquals("test1", testMethod.getName());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
