package utils;

import java.io.File;
import java.net.URL;
import java.net.URLClassLoader;


import prepass.TestMethod;

public class Utils {

    public static TestMethod getTestMethod(String name) {
        try {
            Class<?> testClass = new URLClassLoader(new URL[]{
                    new File("triangle/build/main/").toURI().toURL(),
                    new File("triangle/build/test/").toURI().toURL()
            }).loadClass("triangle.test.TriangleTest");

            return new TestMethod(testClass, name, "triangle/build/main");
        } catch (Exception e) {
            e.printStackTrace();
            throw(new RuntimeException(e));
        }
    }
}
