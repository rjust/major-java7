package major.mml;

import java.util.List;

import com.sun.tools.javac.mutation.operator.TreeMutationProvider;

public interface Statement {
    public boolean execute(TreeMutationProvider tree, List<String> flatname);
}
