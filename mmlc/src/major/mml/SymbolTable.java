package major.mml;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.antlr.runtime.RecognitionException;
import org.antlr.runtime.TokenStream;

public class SymbolTable {
    private Map<String, Set<String>> opLists = new HashMap<String, Set<String>>();
    private Map<String, List<String>> flatNames = new HashMap<String, List<String>>();
    private Map<String, UserDefinedOperator> userDefOperators = new HashMap<String, UserDefinedOperator>();

    private boolean failed = false;

    public boolean failed(){
        return failed;
    }

    public void setFailed(){
        failed=true;
    }

    public void defVarFlatname(String ident, List<String> flatname, TokenStream input) throws RecognitionException{
        if(flatNames.containsKey(ident)){
            failed = true;
            throw new VarDefException(input, "Variable "+ident+" has already been defined!");
        }
        flatNames.put(ident, flatname);
    }

    public void defVarSet(String ident, Set<String> opList, TokenStream input) throws RecognitionException {
        if(opLists.containsKey(ident)) {
            failed = true;
            throw new VarDefException(input, "Variable "+ident+" has already been defined!");
        }
        opLists.put(ident, opList);
    }

    public void defOp(String ident, UserDefinedOperator op, TokenStream input) throws VarDefException{
        if(userDefOperators.containsKey(ident)){
            failed=true;
            throw new VarDefException(input, "Operator "+ident+" has already been defined!");
        }
        userDefOperators.put(ident, op);
    }

    public UserDefinedOperator getOperator(String ident, TokenStream input) throws VarDefException{
        if(! userDefOperators.containsKey(ident)){
            failed = true;
            throw new VarDefException(input, "Reference to undefined Operator "+ident+"!");
        }
        return userDefOperators.get(ident);
    }

    public List<String> getFlatname(String ident, TokenStream input) throws VarDefException{
        if(! flatNames.containsKey(ident)){
            failed = true;
            throw new VarDefException(input, "Reference to undefined Variable "+ident+"!");
        }
        return flatNames.get(ident);
    }

    public Set<String> getOpSet(String ident, TokenStream input) throws VarDefException{
        if(! opLists.containsKey(ident)){
            failed = true;
            throw new VarDefException(input, "Reference to undefined Variable "+ident+"!");
        }
        return opLists.get(ident);
    }

}
