grammar Mml;

options {
  language = Java;
  k=2;
}

tokens {
  // Keywords
  BIN         = 'BIN'   ;
  UNR         = 'UNR'   ;

  LIT         = 'LIT'   ;

  DEL         = 'DEL'   ;

  ORU         = 'ORU'   ;
  LVR         = 'LVR'   ;
  EVR         = 'EVR'   ;
  AOR         = 'AOR'   ;
  COR         = 'COR'   ;
  ROR         = 'ROR'   ;
  SOR         = 'SOR'   ;
  LOR         = 'LOR'   ;
  STD         = 'STD'   ;

  UNDERSCORE  = '_' ;

  // special operators for ROR, COR, and LOR
  LHS         = 'LHS'   ;
  RHS         = 'RHS'   ;

  FALSE       = 'FALSE' ;
  TRUE        = 'TRUE'  ;

  // special operators for STD
  DEL_ASSIGN      = 'ASSIGN';
  DEL_INC         = 'INC'   ;
  DEL_DEC         = 'DEC'   ;
  DEL_CALL        = 'CALL'  ;
  DEL_BREAK       = 'BREAK' ;
  DEL_CONT        = 'CONT'  ;
  DEL_RETURN      = 'RETURN';

  // supported types for LIT
  LIT_BOOLEAN     = 'BOOLEAN';
  LIT_NUMBER      = 'NUMBER' ;
  LIT_STRING      = 'STRING' ;

  // operators and other special chars
  AND                     = '&'        ;
  AND_ASSIGN              = '&='       ;
  ARROW                   = '->'       ;
  ASSIGN                  = '='        ;
  AT                      = '@'        ;
  BIT_SHIFT_RIGHT         = '>>>'      ;
  BIT_SHIFT_RIGHT_ASSIGN  = '>>>='     ;
  COLON                   = ':'        ;
  COMMA                   = ','        ;
  DEC                     = '--'       ;
  DELIM                   = '"'        ;
  DIV                     = '/'        ;
  DIV_ASSIGN              = '/='       ;
  DOLLAR                  = '$'        ;
  DOT                     = '.'        ;
  DOTSTAR                 = '.*'       ;
  ELLIPSIS                = '...'      ;
  EQUAL                   = '=='       ;
  GREATER_OR_EQUAL        = '>='       ;
  GREATER_THAN            = '>'        ;
  INC                     = '++'       ;
  LBRACK                  = '['        ;
  LCURLY                  = '{'        ;
  LESS_OR_EQUAL           = '<='       ;
  LESS_THAN               = '<'        ;
  LOGICAL_AND             = '&&'       ;
  LOGICAL_NOT             = '!'        ;
  LOGICAL_OR              = '||'       ;
  LPAREN                  = '('        ;
  MINUS                   = '-'        ;
  MINUS_ASSIGN            = '-='       ;
  MOD                     = '%'        ;
  MOD_ASSIGN              = '%='       ;
  NOT                     = '~'        ;
  NOT_EQUAL               = '!='       ;
  OR                      = '|'        ;
  OR_ASSIGN               = '|='       ;
  PLUS                    = '+'        ;
  PLUS_ASSIGN             = '+='       ;
  QUESTION                = '?'        ;
  RBRACK                  = ']'        ;
  RCURLY                  = '}'        ;
  RPAREN                  = ')'        ;
  SEMI                    = ';'        ;
  SHIFT_LEFT              = '<<'       ;
  SHIFT_LEFT_ASSIGN       = '<<='      ;
  SHIFT_RIGHT             = '>>'       ;
  SHIFT_RIGHT_ASSIGN      = '>>='      ;
  STAR                    = '*'        ;
  STAR_ASSIGN             = '*='       ;
  XOR                     = '^'        ;
  XOR_ASSIGN              = '^='       ;
}

@header{
package major.mml.antlr;

import com.sun.tools.javac.mutation.operator.IMutationProvider;
import com.sun.tools.javac.mutation.operator.TreeMutationProvider;
import com.sun.tools.javac.mutation.operator.IMutationProvider.MutationOperator;
import com.sun.tools.javac.mutation.operator.IMutationProvider.MutationOperatorType;

import major.mml.*;

import java.util.Set;
import java.util.TreeSet;
import java.util.List;
import java.util.Collections;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
}

@members{
private TreeMutationProvider tree = new TreeMutationProvider(new TreeMutationProvider.TreeNode(""));
public TreeMutationProvider getTreeMutationProvider(){return tree; }
private SymbolTable tab = new SymbolTable();
public SymbolTable getSymbolTable(){ return tab; }
private boolean error=false;
public boolean error(){return error;}

private Pattern pFlatname = Pattern.compile(
                    "[a-zA-z_]\\w*" +
                    "(\\.([a-zA-z_][\\w]*))*" +
                    "(\\$(([a-zA-z_][\\w]+)|\\d+))*" +
                    "(\\@(([a-zA-z_][\\w]+)|<init>|<clinit>))?"
                   );

private List<String> parseFlatname(String str, TokenStream input) throws InvalidFlatnameException {
    Matcher m = pFlatname.matcher(str);
    if (!m.matches()) {
        return parseScope(str, input);
    }
    List<String> l = new ArrayList<>();
    for (String s : str.split("[.$@]")) {
        l.add(s);
    }
    return l;
}

private Pattern pType = Pattern.compile(
                    "[a-zA-z_]\\w*" +
                    "(\\.([a-zA-z_][\\w]*))*" +
                    "(\\$(([a-zA-z_][\\w]+)|\\d+))*" +
                    "(::" +
                    "[a-zA-z_]\\w*" +
                    "(\\(" +
                    "([^,]+(,[^,]+)*)?" +
                    "\\))?)?"
                   );

private List<String> parseScope(String str, TokenStream input) throws InvalidFlatnameException {
    Matcher m = pType.matcher(str);
    if (!m.matches()) {
        error = true;
        throw new InvalidFlatnameException(input, "invalid scope: " + str);
    }
    // Add a separator between the method name and the formal parameters  
    str = str.replace("(", "::(");
    List<String> l = new ArrayList<>();
    for (String s : str.split("[.$@]|::")) {
        l.add(s);
    }
    return l;
}

private void checkType(String str, TokenStream input) throws InvalidFlatnameException {
    Matcher m = pType.matcher(str);
    if (!m.matches()) {
        error = true;
        throw new InvalidFlatnameException(input, "invalid flatname: " + str);
    }
}

}

@lexer::header{
  package major.mml.antlr;
}

fragment DIG  : '0'..'9';
fragment HEX  : '0''x' ('a'..'f' | 'A'..'F' | DIG)+;
fragment INT  : DIG+
              ;

INT_LIT       : (PLUS|MINUS)? INT | HEX
              ;

fragment
ESC_QUOTE     : '\\"' {setText("\"");}
              ;

STRING_LIT
@init{StringBuilder buf = new StringBuilder();}
              :
              '"'
               (
                 ESC_QUOTE {buf.append('"');} |
                 normal=~('"'|'\\'|'\n'|'\r') {buf.appendCodePoint(normal);} |
                 '\\n' {buf.append('\n');} |
                 '\\r' {buf.append('\r');}
                )*
              '"'
               {setText(buf.toString());}
              ;

fragment
ESC_SINGLE    : '\'' '\\\'' '\'' {setText("\'");} 
              |'\'\\n\'' {setText("\n");}
              |'\'\\r\'' {setText("\r");}
              ;

fragment
NO_ESC_SINGLE : '\'' ~('\'') '\'' {setText(getText().substring(1, getText().length()-1));}
              ;

CHAR_LIT      : ESC_SINGLE | NO_ESC_SINGLE
              ;

FLOAT_LIT     : (PLUS|MINUS)?
              ( ('0'..'9')* DOT ('0'..'9')*
                | ('0'..'9')* DOT ('0'..'9')+ ('e'|'E')(PLUS|MINUS)? ('0'..'9')+
                | '0'
                | '1'..'9'DIG* (('e'|'E')(PLUS|MINUS)? ('0'..'9')+)?
              ) ('d'|'D'|'f'|'F')?
              ;

BOOL_LIT      : 'true'|'false'
              ;


IDENT_START   : ('a'..'z'|'A'..'Z'|UNDERSCORE)
              ;

IDENT         : IDENT_START (IDENT_START | INT_LIT)*
              ;


COMMENT
              :   '//' ~('\n'|'\r')*  ('\r\n' | '\r' | '\n')? {skip(); }
              ;

WS            : (' ' | '\r' | '\t' | '\u000C' | '\n' ) {skip(); }
              ;

INIT_NAME     : LESS_THAN 'init' GREATER_THAN
              ;

/*
 * PARSER SECTION
 */

script        : stmt* ;

stmt          : vardef SEMI
              | call SEMI
              | replace_stmt SEMI
              | delete_stmt SEMI
              | lit_stmt SEMI
              | opdef
              | COMMENT
              | SEMI
              ;

replace_stmt  @init{
                List<String> flatname=Collections.<String>emptyList();
                Set<String> opList=Collections.<String>emptySet();
              }

              : scope_prefix mut_kind LPAREN op RPAREN stmt_scope {flatname=$stmt_scope.fn; } ARROW
                (l=op_list
                {
                  opList=$l.opList;
                }
                | IDENT
                {
                  opList=tab.getOpSet($IDENT.text, input);
                }
                )
                {
                  error |= !tree.setMutationOperators(flatname, $mut_kind.type, $op.text, opList, $scope_prefix.replace);
                }
              ;

// DEL statements do not require a replacement list -- NP-OP is the implicit replacement
delete_stmt  @init{
                List<String> flatname=Collections.<String>emptyList();
                Set<String> opList=Collections.<String>singleton("NO-OP");
              }

              : scope_prefix DEL LPAREN del_op RPAREN stmt_scope {flatname=$stmt_scope.fn; }
                {
                  error |= !tree.setMutationOperators(flatname, MutationOperatorType.DEL, $del_op.text, opList, $scope_prefix.replace);
                }
              ;

// LIT statements do currently not support a replacement list
lit_stmt  @init{
                List<String> flatname=Collections.<String>emptyList();
                Set<String> opList=Collections.<String>singleton("ALL");
              }

              : scope_prefix LIT LPAREN lit_type RPAREN stmt_scope {flatname=$stmt_scope.fn; }
                {
                  error |= !tree.setMutationOperators(flatname, MutationOperatorType.LIT, $lit_type.text, opList, $scope_prefix.replace);
                }
              ;

scope_prefix returns [boolean replace] @init{replace=true;} : PLUS{replace=false; } |(|LOGICAL_NOT){replace=true; }
              ;

stmt_scope returns [List<String> fn]
              @init{
                $fn=Collections.<String>emptyList();
              }
              :
              |
              LESS_THAN
              (
              IDENT {$fn=tab.getFlatname($IDENT.text, input); }
              | str=STRING_LIT {$fn=parseFlatname($str.text, input); }
              ) GREATER_THAN
              ;
/*
flatname returns [List<String> asList]
              @init{
                $asList = new ArrayList<String>();
              }
              : i1=IDENT{$asList.add($i1.text);}
                (DOT i2=IDENT{$asList.add($i2.text);})*
                (DOLLAR (i3=IDENT{$asList.add($i3.text);} | INT_LIT{$asList.add($INT_LIT.text);}) )*
                (AT (i4=IDENT{$asList.add($i4.text);} | INIT_NAME{$asList.add($INIT_NAME.text);}) )?
              ;
*/

mut_kind returns [MutationOperatorType type]
              : BIN {$type=MutationOperatorType.BIN; }
              | UNR {$type=MutationOperatorType.UNR; }
              ;
mut_op returns [MutationOperator op, boolean remove]
              : (PLUS {$remove=false; }| MINUS {$remove=true; })?
              ( AOR {$op=MutationOperator.AOR; }
              | LOR {$op=MutationOperator.LOR; }
              | COR {$op=MutationOperator.COR; }
              | ROR {$op=MutationOperator.ROR; }
              | SOR {$op=MutationOperator.SOR; }
              | ORU {$op=MutationOperator.ORU; }
              | LVR {$op=MutationOperator.LVR; }
              | EVR {$op=MutationOperator.EVR; }
              | STD {$op=MutationOperator.STD; })
              ;

op_list returns [Set<String> opList]
              @init{
                $opList = new TreeSet<String>();
              }
              : LCURLY opFirst=op {$opList.add($opFirst.text); } (',' opRest=op
              {
                if($opList.contains($opRest.text)){
                  tab.setFailed();
                  throw new VarDefException(input, "Redundant operator "+$opRest.text+" in replacement List!");
                }
                $opList.add($opRest.text);
              })* RCURLY
              ;

op            : STAR | DIV | MOD | PLUS | MINUS                                                   // AOR/ORU
              | LESS_THAN | LESS_OR_EQUAL | GREATER_THAN | GREATER_OR_EQUAL | EQUAL | NOT_EQUAL   // ROR
              | LOGICAL_AND | LOGICAL_OR                                                          // COR
              | XOR | AND | OR                                                                    // LOR
              | SHIFT_LEFT | SHIFT_RIGHT | BIT_SHIFT_RIGHT                                        // SOR
              | NOT | LOGICAL_NOT                                                                 // ORU
              | INC | DEC                                                                         // ORU
              | LHS | RHS | FALSE | TRUE                                                          // ROR/COR
              ;

assign_op     : PLUS_ASSIGN|MINUS_ASSIGN|DIV_ASSIGN|MOD_ASSIGN|STAR_ASSIGN
              | AND_ASSIGN|OR_ASSIGN|XOR_ASSIGN
              | SHIFT_LEFT_ASSIGN | SHIFT_RIGHT_ASSIGN | BIT_SHIFT_RIGHT_ASSIGN
              | ASSIGN
              ;

del_op        : DEL_ASSIGN|DEL_CALL|DEL_INC|DEL_DEC|DEL_BREAK|DEL_CONT|DEL_RETURN
              ;

lit_type      : LIT_BOOLEAN|LIT_NUMBER|LIT_STRING
              ;	

/*
expr          : INT_LIT
              | FLOAT_LIT
              | STRING_LIT
              ;
*/

vardef        : id=IDENT '='
                ( str=STRING_LIT {tab.defVarFlatname($id.text, parseFlatname($str.text, input), input); }
                | op_list {tab.defVarSet($id.text, $op_list.opList, input); }
                )
              ;

opdef         @init{
                List<Statement> stmtList = new ArrayList<Statement>();
              }
              : IDENT LCURLY (op_block {stmtList.add($op_block.stmt); })* RCURLY
              {
                tab.defOp($IDENT.text, new UserDefinedOperator($IDENT.text, stmtList), input);
              }
              ;

op_block returns [Statement stmt]
              @init{
                Set<String> opList=Collections.<String>emptySet();
              }
              : scope_prefix mut_kind LPAREN op RPAREN ARROW
                (   l=op_list {opList=$l.opList; }
                  | IDENT {opList=tab.getOpSet($IDENT.text, input); }
                ) SEMI
                {
                  stmt = new ReplacementStatement($mut_kind.type, $op.text, opList, $scope_prefix.replace);
                }
              | scope_prefix DEL LPAREN del_op RPAREN SEMI
                {
                  opList=Collections.<String>singleton("NO-OP");
                  stmt = new ReplacementStatement(MutationOperatorType.DEL, $del_op.text, opList, $scope_prefix.replace);
                }
              | scope_prefix LIT LPAREN lit_type RPAREN SEMI
                {
                  opList=Collections.<String>singleton("ALL");
                  stmt = new ReplacementStatement(MutationOperatorType.LIT, $lit_type.text, opList, $scope_prefix.replace);
                }

              | mut_op SEMI {stmt = new CallStatement($mut_op.op, $mut_op.remove); }
              | IDENT SEMI {stmt = tab.getOperator($IDENT.text, input); }
              | COMMENT
              | SEMI
              ;

call          : mut_op stmt_scope {error |= !tree.enableOperator($stmt_scope.fn, $mut_op.op, $mut_op.remove); }
              | IDENT stmt_scope {error |= !tab.getOperator($IDENT.text, input).execute(tree, $stmt_scope.fn); }
              ;
