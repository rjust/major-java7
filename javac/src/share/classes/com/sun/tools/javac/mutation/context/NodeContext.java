package com.sun.tools.javac.mutation.context;

import com.sun.source.tree.Tree.Kind;
import com.sun.tools.javac.tree.JCTree;
import com.sun.tools.javac.tree.JCTree.JCBlock;
import com.sun.tools.javac.tree.JCTree.JCExpressionStatement;
import com.sun.tools.javac.tree.JCTree.JCStatement;

/**
 * A representation of program context in terms of node and edge labels.
 */
public class NodeContext {
    public static final String CONTEXT_SEP = ":";

    private JCTree node;
    private EdgeLabel edgeLabel;
    private boolean hasLiteralChild = false;
    private boolean hasVariableChild = false;
    private boolean hasOperatorChild = false;

    public enum EdgeLabel{BODY, COND, INIT, STEP, EXPR, LHS, RHS, VAR, INDEXED, INDEX, DIMS, ELEMS, TRUEPART, FALSEPART, THENPART, ELSEPART};

    public NodeContext(JCTree node, EdgeLabel edgeLabel) {
        this.node = node;
        this.edgeLabel = edgeLabel;
    }

    public Kind getKind() {
        // Return the kind of the wrapped expression for any ExpressionStatement
        if (node.getKind() == Kind.EXPRESSION_STATEMENT) {
            return ((JCExpressionStatement)node).expr.getKind();
        }
        return node.getKind();
    }

    public EdgeLabel getEdgeLabel() {
        return edgeLabel;
    }

    public boolean isStmtNode() {
        return (node instanceof JCStatement);
    }

    public boolean isBlockNode() {
        return (node instanceof JCBlock);
    }

    public boolean hasLiteralChild() {
        return hasLiteralChild;
    }

    public boolean hasVariableChild() {
        return hasVariableChild;
    }

    public boolean hasOperatorChild() {
        return hasOperatorChild;
    }

    public void setHasLiteralChild(boolean hasLiteralChild) {
        this.hasLiteralChild = hasLiteralChild;
    }

    public void setHasVariableChild(boolean hasVariableChild) {
        this.hasVariableChild = hasVariableChild;
    }

    public void setHasOperatorChild(boolean hasOperatorChild) {
        this.hasOperatorChild = hasOperatorChild;
    }

    @Override
    public String toString() {
        if (edgeLabel != null) {
            return node.getKind().toString() + CONTEXT_SEP + edgeLabel.name();
        } else {
            return node.getKind().toString();
        }
    }
}
