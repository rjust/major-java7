\major provides a default analyzer, which extends the Apache Ant junit task.
This analyzer supports JUnit 3 and 4 tests.
%
Note that \major does currently not support forking a JVM when executing JUnit
tests, meaning that the \code{fork} option must not be set to \code{true}.

\section{Setting up a mutation analysis target}
Most software projects that are build with Apache Ant provide a \code{test}
target, which executes the corresponding unit tests. Even if no such target
exists, it can be easily set up to execute a set of given unit tests. The
following code snippet shows an exemple \code{test} target (See
\url{http://ant.apache.org/manual/Tasks/junit.html} for a detailed description
of the options used in the \code{junit} task): 
%
\begin{Verbatim}[baselinestretch=1.0,fontsize=\small]
<target name="test" description="Run all unit test cases">         
    <junit printsummary="true"                                    
             showoutput="true"                                      
          haltonfailure="true">                              
        
        <formatter type="plain" usefile="true"/>                  
        <classpath path="bin"/>                                                                                                       
        <batchtest fork="no">                                      
            <fileset dir="test">                                   
                <include name="**/*Test*.java"/>                   
            </fileset>                                             
        </batchtest>                                               
    </junit>                                                       
</target>
\end{Verbatim}
%
To enable mutation analysis in \major's enhanced junit task, the
option \code{mutationAnalysis} has to be set to \code{true}. For clarity, it is
better to duplicate and adapt an existing \code{test} target, instead of
parameterizing the existing one
(See Section~\ref{sec:ant/optimize} for recommended configurations):
%
\newpage
\begin{Verbatim}[baselinestretch=1.0,fontsize=\small]
<target name="mutation.test" description="Run mutation analysis">         
    <junit printsummary="false"                                    
             showoutput="false"                                      
          haltonfailure="true"

       mutationAnalysis="true"
            summaryFile="summary.csv" 
      mutantDetailsFile="details.csv">

        <classpath path="bin"/> 
        <batchtest fork="no">                                      
            <fileset dir="test">                                   
                <include name="**/*Test*.java"/>                   
            </fileset>                                             
        </batchtest>                                               
    </junit>                                                       
</target>
\end{Verbatim}

\section{Configuration options for mutation analysis}
Table~\ref{tab:ant/options} summarizes the additional options for \major's
default analyzer. Please refer to the official documentation of the junit task
for all other options.
\input{sections/ant/tableOptions}

\section{Performance optimization}\label{sec:ant/optimize}
During the mutation analysis process, the provided JUnit tests are repeatedly
executed. For performance reasons, consider the following when setting
up the mutation analysis target for a JUnit test suite:
\begin{itemize*}
    \item Turn off logging output (options \code{showsummary}, \code{showoutput}, etc.)
    \item Do not use result formatters (nested task \code{formatter}, especially the \code{usefile} option)
\end{itemize*}
Due to frequent class loading and thread executions, the following JVM options
are recommended, in particular for large projects:
\begin{itemize*}
    \item \code{-XX:ReservedCodeCacheSize=256M}
    \item \code{-XX:MaxPermSize=1G}
\end{itemize*}
